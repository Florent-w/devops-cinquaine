DevOps du site Cinquaine

Auteurs :

Florent WELTMANN
Mehdi DARCY
Abdoula JAITEH


I) Présentation du site

Le projet Cinquaine se base sur le célèbre site Fiverr d’où son nom. Il propose à des particuliers de pouvoir s’échanger des services de tout type (jardinage, promener son chien, cours particulier…)

Dans la page d’accueil se trouvent les différents services proposés par chaque utilisateur. Il est également possible de faire une recherche par catégorie ou par mot clé. Les visiteurs peuvent s’inscrire et se connecter. Ils pourront alors proposer un service ou bien en acheter. Car oui, il y a une monnaie présente sur le site. Ce sont des points achetables avec de l’argent réel (sur le site actuel, la phase de paiement est simulée puisque n’étant pas un projet professionnel).

Acheter un service fera dépenser des points. En revanche pour le vendeur du service, celui-ci gagnera des points qu’il pourra utiliser pour acheter d’autres services. (Il est possible donc pour un vendeur de créer, supprimer et de modifier les services, de même que les administrateurs).

II) Présentation de l'infrastructure

Le site a été codé principalement en PHP avec du Jquery, du HTML et du CSS pour la partie front et une base de données MySQL. Il utilise l’architecture Modèle - Vue - Contrôleur pour pouvoir bien structurer le code et les différentes parties. L’outil de gestion de base de données utilisé est PhpMyAdmin.

Lien vers la partie DevOps du site : https://gitlab.com/Florent-w/devops-cinquaine
Lien vers le Gitlab du code du site : https://gitlab.com/Florent-w/Cinquaine

III) Présentation du DevOps

Ansible est utilisé avec un playbook pour télécharger et configurer la plupart des outils nécessaires pour la plateforme.

Il faudra lancer la commande :

"git clone https://gitlab.com/Florent-w/devops-cinquaine.git projet
sudo ansible-playbook -i projet/hosts projet/playbook_web.yml" sur un système Unix. Il vous sera demandé votre mot de passe. La partie DevOps sera alors téléchargé (Ansible) puis
Ansible lancera les différentes tâches présentes dans le playbook.

Liste non exhaustive des tâches : 

- Installation des dépendances de Docker
- Mise à jour cache apt
- Installation de Docker
- Installation de Python
- Ajout du dépôt de Gitlab Runner
- Installation de Gitlab Runner
- Création de différents volumes pour pouvoir accéder à différents répertoires entre les conteneurs
- Clonage du projet Cinquaine depuis Gitlab avec changement des permissions sur le dossier
- Suppression des conteneurs existants
- Lancement du conteneur PHP 7.4 avec une image Docker personnalisée mise précédemment dans le registre Docker (Cette image possède en plus apt-get, nano, pdo). Elle est disponible au registre florentwe/image_php. Le conteneur se lancera sur le 8080.
- Lancement du conteneur MySQL 5.7 avec configuration d’un mot de passe personnalisé. Le conteneur se lancera sur le port 3306.
- Lancement du conteneur PhpMyAdmin, qui pourra être utile pour gérer la base de données. Le conteneur se lancera sur le port 8081. Une fois entré sur la page de PhpMyAdmin, il faudra mettre l’ip du conteneur MySQL qui est disponible en lançant la commande “docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mysql_container
- L’ip sera alors indiqué et il faudra mettre l’ip dans la partie serveur (172.17.0.3 dans les tests). - L’utilisateur est root et le mot de passe est celui défini plus haut dans le playbook (votre_mot_de_passe)
- Suppression de la base de données si il y en a déjà une vu que c’est une nouvelle installation.
- Copie du fichier SQL dans le container mysql qui contient la structure de la base de données. - Insertion via ce fichier sql.
- Lancement d’un seeder qui va insérer les données en base (Le site a été codé de tel sorte que des données de test s’injectent une fois que la page http://localhost/index.php?action=seedDatabase&controller=controllerSeeder est lancée.
- Ajout d’un cron qui va cloner le dossier du site internet s'ils n’existent pas ou restaurer les fichiers du site internet toutes les minutes s'ils en manquent.

Les différentes tâches prennent environ 30 secondes à se lancer (sans compter les différents temps de téléchargement). Si il n’y a pas eu d’erreurs, trois conteneurs sont lancés dans Docker : php_container, mysql_container, phpmyadmin_container.

Le site devrait être disponible à cette adresse en local : http://localhost:8080/

Quelques comptes insérés via le seeder :

Utilisateur, Mot de passe
jeanbon, cochonou
pierrepaul, jacques
robertjardineur, petunias12345

Vous pouvez vous connecter avec ceux-là pour tester ou bien créer votre propre compte avec le formulaire d’inscription.

Gitlab CI/CD : 

Un fichier .gitlab-ci.yml est mis à la racine du projet, celui-ci sera lancé à chaque commit. Mais tout d’abord, nous avons enregistré deux runners locaux dans l’infrastructure : un avec le tag “local” qui aura comme executor : docker et un avec le tag “local-dsi” qui aura comme executor “shell”. Cela sera utile de faire des runners locaux si il y a besoin d’accéder aux conteneurs locaux.

Trois stages sont lancés à chaque commit : 

Les tests unitaires avec une image php et phpunit qui permettront de vérifier le bon fonctionnement des fonctions ainsi que leurs bons déroulements.
Les tests fonctionnels via selenium. Un enregistrement du comportement attendu sur le site a été enregistré via le plugin de selenium sur Google Chrome. Les tests sont ensuite lus via selenium-side-runner.
Enfin, le déploiement du nouveau code via le lancement du fichier test.sh qui va regarder si le répertoire est vide, si oui, il y aura un clone du projet depuis gitlab, si il n’est pas vide, il y aura un git pull pour mettre à jour le projet dans container php, ce qui sera visible en allant à l’url : http://localhost:8080/

Des emails sont envoyés automatiquement pour dire à l’auteur du commit si le CI/CD s’est bien passé.

Cron PHP :

Un script en php a été codé pour tourner toutes les minutes via un cron (dans un environnement professionnel, ce serait trop fréquent mais dans le cadre de la démonstration, il est intéressant de voir le script tourné sans avoir à attendre).

Grâce au playbook, la ligne cd \"{{ current_directory }}\" && php cron.php >> cron.err 2>&1 est ajouté automatiquement au crontab. Le script tournera alors toutes les minutes et les erreurs seront indiquées dans le fichier cron.err. Le fichier cron est dans /tmp/mycron.

Le script enregistre les messages dans un fichier cron.log. Il effectue une vérification pour déterminer si des fichiers du projet sont présents. Si aucun fichier n'est trouvé, il clonera le projet web "Cinquaine" dans le conteneur PHP. Dans le cas où des fichiers sont déjà présents et qu’il y a un fichier.git, il effectuera une restauration des fichiers depuis le dernier commit ainsi qu’un "git pull" dans le conteneur PHP si il n’y a pas de changement dans le répertoire local. Ce script est conçu pour répondre à la suppression d'une page de projet. Lorsqu'une page est supprimée, les fichiers correspondants sont téléchargés automatiquement dans la minute qui suit.
