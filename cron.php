<?php
$sharedVolumePath = '/';
$logFilePath = __DIR__ . '/cron.log';

// Ajout d'un message de log pour indiquer la vérification du répertoire Git
$logMessage = 'Vérification du répertoire Git dans le conteneur php_container - ' . date('Y-m-d H:i:s') . PHP_EOL;

file_put_contents($logFilePath, $logMessage, FILE_APPEND);

$cinquainePath = trim(shell_exec('docker exec php_container sh -c "pwd"'));
$logMessage = 'Répertoire actuel dans le conteneur php_container : ' . $cinquainePath . PHP_EOL;
file_put_contents($logFilePath, $logMessage, FILE_APPEND);

// Ajout d'un message de log avec le nombre de fichier dans le répertoire
$logMessage = 'Fichier .git trouvé dans le répertoire : ' . PHP_EOL;
$nbFichier = shell_exec("docker exec php_container sh -c 'ls -A | grep -c '^\.git$");

file_put_contents($logFilePath, $logMessage, FILE_APPEND);
file_put_contents($logFilePath, $nbFichier, FILE_APPEND);

if ($nbFichier > 0) {
    $differenceMessage = 'Fichiers et dossiers différents :' . PHP_EOL;
    $differenceOutput = shell_exec('docker exec php_container sh -c "git diff --name-only --diff-filter=D 2>&1"');
    
    file_put_contents($logFilePath, $differenceMessage, FILE_APPEND);
    file_put_contents($logFilePath, $differenceOutput, FILE_APPEND);

    // Si il y a des différences, on récupère ce qui manque
    if(!empty($differenceOutput)) {
        $recupMessage = 'Récupération des fichiers et des dossiers manquants ' . PHP_EOL;
        $recupOutput = shell_exec('docker exec php_container sh -c "git ls-files --deleted --others | xargs git checkout . 2>&1 && chmod -R 777 ."');

        file_put_contents($logFilePath, $recupMessage, FILE_APPEND);
        file_put_contents($logFilePath, $recupOutput, FILE_APPEND);

    }
    $recupMessage = 'Aucun fichier à récupérer ! ' . PHP_EOL;
    file_put_contents($logFilePath, $recupMessage, FILE_APPEND);
} else {
    $cloneLogMessage = 'Clonage du répertoire Git dans ' . $cinquainePath . PHP_EOL;
    file_put_contents($logFilePath, $cloneLogMessage, FILE_APPEND);

    $cloneOutput = shell_exec('docker exec php_container sh -c "git clone https://gitlab.com/Florent-w/Cinquaine.git . 2>&1"');
    file_put_contents($logFilePath, $cloneOutput, FILE_APPEND);
}
?>