#!/bin/sh
echo "Le script est lancé"
   if [ -z "$(docker exec php_container ls -A /var/shared_volume/)" ]; then
        echo "Le répertoire est vide, clonage du projet"
        docker exec php_container git clone https://gitlab.com/Florent-w/Cinquaine.git /var/shared_volume/ \
        && echo "Clonage réussi !"
    else
        echo "Le répertoire est déjà là, git pull"
        docker exec php_container bash -c "cd /var/shared_volume && git pull" && echo "La commande git pull a réussi !" || echo "La commande git pull a échoué :("
    fi
